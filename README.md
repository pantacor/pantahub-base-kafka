# Pantahub Listen

## Build & Run

```
Syntax: pantahub-base-kafka [--baseurl <baseapiurl>] [--topic <topicname>] [--consumer-group <groupname>] <controller>

$ go build
$ pantahub-base-kafka --baseurl=https://api.pantahub.com --topic=pantabasemgo.pantabase-serv.pantahub_devices --consumer-group=consumer-group-objcontrol-devices devices

kafka-logger: entering loop for consumer group, consumer-group-objcontrol-devices
kafka-logger: joined group consumer-group-objcontrol-devices as member test1@lenovo-ThinkPad-P50 (github.com/segmentio/kafka-go)-4cbdc2ce-5628-4a91-8543-e52c6387ca2d in generation 111
kafka-logger: selected as leader for group, consumer-group-objcontrol-devices
kafka-logger: using 'range' balancer to assign group, consumer-group-objcontrol-device
...

$ go build
$ pantahub-base-kafka --baseurl=https://api.pantahub.com --topic=pantabasemgo.pantabase-serv.pantahub_steps --consumer-group=consumer-group-objcontrol-ste[s] steps
kafka-logger: entering loop for consumer group, consumer-group-objcontrol-steps
kafka-logger: joined group consumer-group-objcontrol-steps as member test2@lenovo-ThinkPad-P50 (github.com/segmentio/kafka-go)-61a39232-8154-4331-b2b8-359f5a00fe54 in generation 100
kafka-logger: selected as leader for group, consumer-group-objcontrol-steps
kafka-logger: using 'range' balancer to assign group, consumer-group-objcontrol-steps

```

## Env Vars:

```
1.PANTAHUB_BASE_URL
2.KAFKA_TOPIC
3.KAFKA_CONSUMER_GROUP
4.KAFKA_HOST
5.KAFKA_PORT
```
