module gitlab.com/pantacor/pantahub-base-kafka

go 1.13

replace github.com/ant0ine/go-json-rest => github.com/asac/go-json-rest v3.3.3-0.20191004094541-40429adaafcb+incompatible

replace github.com/go-resty/resty => gopkg.in/resty.v1 v1.11.0

require (
	github.com/Azure/go-autorest/autorest/date v0.3.0
	github.com/levenlabs/golib v0.0.0-20180911183212-0f8974794783
	github.com/segmentio/kafka-go v0.3.5
	gitlab.com/pantacor/pantahub-base v0.0.0-20200508145928-38aae43563a9
	go.mongodb.org/mongo-driver v1.2.1
	gopkg.in/resty.v1 v1.12.0
)
