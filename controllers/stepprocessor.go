package controllers

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
	_ "github.com/segmentio/kafka-go/snappy"
	"gitlab.com/pantacor/pantahub-base/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/resty.v1"
)

var KafkaStepConsumerGroup = "consumer-group-objcontrol-steps"

var KafkaStepTopic = "pantabasemgo.pantabase-serv.pantahub_steps"

type StepProcessor struct{}

func (s *StepProcessor) HandleMessage(m kafka.Message) error {

	var stepStr interface{}
	var res *resty.Response
	var step map[string]interface{}

	var tmTime primitive.DateTime
	var tm interface{}
	var stepID string

	err := bson.UnmarshalExtJSON([]byte(m.Value), true, &stepStr)
	if err != nil {
		goto err
	}

	err = bson.UnmarshalExtJSON([]byte(stepStr.(string)), false, &step)
	if err != nil {
		goto err
	}

	if step["_id"] == nil || step["_id"] == "" {
		err = errors.New("No step ID")
		goto err
	}

	stepID = step["_id"].(string)

	if stepID == "" {
		err = errors.New("Empty step ID")
		goto err
	}

	tm = step["timemodified"]

	if tm == nil {
		err = errors.New("No timemodified")
		log.Printf("Error processing step event:" + err.Error())
		// stop processing if timemodified doesn't exists
		return nil
	}

	tmTime = tm.(primitive.DateTime)

	if tmTime.Time().IsZero() {
		err = errors.New("Bad Time (zero/not castable?): " + reflect.TypeOf(tm).String())
		log.Printf("Error processing step event:" + err.Error())
		// stop processing if timemodified doesn't exists
		return nil
	}

	res, err = ChangeStepCallback(stepID, tmTime, m.Offset)
	if err != nil {
		goto err
	}

	if res.StatusCode() != http.StatusOK && res.StatusCode() != http.StatusNotModified {
		goto err
	}

	return nil

err:
	fmt.Printf("Error for message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
	if res != nil {
		var body string
		if res.Body() != nil {
			body = string(res.Body())
		}
		return errors.New("Got bad response code:" + strconv.Itoa(res.StatusCode()) + " body: " + body)
	}
	return errors.New("Error processing step event:" + err.Error())
}

// ChangeStepCallback : Change Step Callback
func ChangeStepCallback(stepID string, dateTime primitive.DateTime, offset int64) (
	*resty.Response,
	error,
) {

	stepTime := dateTime.Time()

	APIEndPoint := BaseURL + "/callbacks/steps/" + stepID

	request := resty.SetDisableWarn(true).R()
	request.SetBasicAuth("saadmin", utils.GetEnv(utils.EnvPantahubSaAdminSecret))
	request.SetQueryParams(map[string]string{
		"timemodified": stepTime.Format(time.RFC3339Nano),
	})
	fmt.Printf("step#%d: PUT %s?timemodified=%s ", offset, APIEndPoint, stepTime.Format(time.RFC3339Nano))
	res, err := request.Put(APIEndPoint)
	if err != nil {
		goto out
	}
	fmt.Printf("%d\n", res.StatusCode())
	return res, err
out:
	fmt.Printf("err(%s)\n", err.Error())
	return res, err
}

func (s *StepProcessor) Run() {
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:         []string{GetKafkaURL()},
		GroupID:         KafkaStepConsumerGroup,
		Topic:           KafkaStepTopic,
		MinBytes:        10e3,            // 10KB
		MaxBytes:        10e6,            // 10MB
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	})

	ctx := context.Background()
	for {
		m, err := r.FetchMessage(ctx)
		if err != nil {
			break
		}
		err = s.HandleMessage(m)
		if err != nil {
			fmt.Println("Breaking because of error:" + err.Error())
			break
		}
		r.CommitMessages(ctx, m)
	}
	r.Close()
}

func NewStepProcessor(topic, consumerGroup string) KafkaTopicController {
	if topic != "" {
		KafkaStepTopic = topic
	}
	if consumerGroup != "" {
		KafkaStepConsumerGroup = consumerGroup
	}
	return &StepProcessor{}
}
