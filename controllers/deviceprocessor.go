package controllers

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
	_ "github.com/segmentio/kafka-go/snappy"
	"gitlab.com/pantacor/pantahub-base/devices"
	"gitlab.com/pantacor/pantahub-base/utils"
	"go.mongodb.org/mongo-driver/bson"
	"gopkg.in/resty.v1"
)

var KafkaDeviceConsumerGroup = "consumer-group-objcontrol-devices"

var KafkaDeviceTopic = "pantabasemgo.pantabase-serv.pantahub_devices"

type DeviceProcessor struct{}

func (s *DeviceProcessor) HandleMessage(m kafka.Message) error {

	var deviceStr interface{}
	err := bson.UnmarshalExtJSON([]byte(m.Value), true, &deviceStr)
	if err != nil {
		return err
	}
	var device devices.Device
	err = bson.UnmarshalExtJSON([]byte(deviceStr.(string)), false, &device)
	if err != nil {
		return err
	}
	if device.ID.IsZero() {
		return errors.New("Invalid device ID:" + device.ID.Hex())
	}
	if device.TimeModified.IsZero() {
		//Committing
		return nil
	}

	res, err := ChangeDeviceCallback(device, m.Offset)
	if err != nil {
		return err
	}
	if res.StatusCode() != http.StatusOK && res.StatusCode() != http.StatusNotModified {
		fmt.Printf("Error for message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		var body string
		if res.Body() != nil {
			body = string(res.Body())
		}
		return errors.New("Got bad response code:" + strconv.Itoa(res.StatusCode()) + " body: " + body)
	}
	return nil
}

// ChangeDeviceCallback : Change Device Callback
func ChangeDeviceCallback(device devices.Device, offset int64) (
	*resty.Response,
	error,
) {
	APIEndPoint := BaseURL + "/callbacks/devices/" + device.ID.Hex()

	fmt.Printf("device#%d: PUT %s ", offset, APIEndPoint)
	request := resty.SetDisableWarn(true).R()
	request.SetBasicAuth("saadmin", utils.GetEnv(utils.EnvPantahubSaAdminSecret))
	request.SetQueryParams(map[string]string{
		"timemodified": device.TimeModified.Format(time.RFC3339Nano),
	})

	res, err := request.Put(APIEndPoint)
	if err != nil {
		goto out
	}

	fmt.Printf("%d\n", res.StatusCode())
	return res, err
out:
	fmt.Printf("err(%s)\n", err.Error())
	return res, err
}

func (s *DeviceProcessor) Run() {
	// make a new reader that consumes from topic-A
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:         []string{GetKafkaURL()},
		GroupID:         KafkaDeviceConsumerGroup,
		Topic:           KafkaDeviceTopic,
		MinBytes:        10e3,            // 10KB
		MaxBytes:        10e6,            // 10MB
		MaxWait:         1 * time.Second, // Maximum amount of time to wait for new data to come when fetching batches of messages from kafka.
		ReadLagInterval: -1,
	})

	ctx := context.Background()
	for {
		m, err := r.FetchMessage(ctx)
		if err != nil {
			break
		}
		err = s.HandleMessage(m)
		if err != nil {
			fmt.Println("Breaking because of error:" + err.Error())
			break
		}
		r.CommitMessages(ctx, m)
	}
	r.Close()
}

func NewDeviceProcessor(topic, consumerGroup string) KafkaTopicController {
	if topic != "" {
		KafkaDeviceTopic = topic
	}
	if consumerGroup != "" {
		KafkaDeviceConsumerGroup = consumerGroup
	}
	return &DeviceProcessor{}
}
