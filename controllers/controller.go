package controllers

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/segmentio/kafka-go"
)

// BaseURL is the base api URL
var BaseURL = os.Getenv("PANTAHUB_BASE_URL")

type KafkaTopicController interface {
	HandleMessage(msg kafka.Message) error
	Run()
}

// GetKafkaURL : Get Kafka URL
func GetKafkaURL() string {
	host := os.Getenv("KAFKA_HOST")
	if host == "" {
		host = "localhost"
	}
	port := os.Getenv("KAFKA_PORT")
	if port == "" {
		port = "9092"
	}
	KafkaURL := host + ":" + port
	return KafkaURL
}

// PrettyPrint : Pretty Print
func PrettyPrint(value interface{}) {
	b, err := json.MarshalIndent(value, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(b))
}
