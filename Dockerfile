FROM golang:alpine3.11 as builder

ENV GO111MODULE=on

RUN apk add -U --no-cache \
    git \
    curl \
    build-base

WORKDIR /app/
COPY . .

RUN go get -d -v ./... \
    && go install -v ./...

FROM alpine

RUN apk update; apk add ca-certificates

COPY env.default /opt/ph/bin/
COPY --from=builder /go/bin/pantahub-base-kafka /opt/ph/bin/

ENTRYPOINT [ "/opt/ph/bin/pantahub-base-kafka" ]
