package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/pantacor/pantahub-base-kafka/controllers"
)

func main() {

	controllers.BaseURL = *flag.String("baseurl", os.Getenv("PANTAHUB_BASE_URL"), "--baseurl <baseapiurl>")

	topic := flag.String("topic", os.Getenv("KAFKA_TOPIC"), "--topic <topicname>")

	consumerGroup := flag.String("consumer-group", os.Getenv("KAFKA_CONSUMER_GROUP"), "--consumer-group <groupname>")

	help := flag.Bool("help", false, "--help")

	flag.Parse()

	controllersAvailable := []string{"devices", "steps"}

	if *help || len(flag.Args()) == 0 || !Contains(controllersAvailable, flag.Args()[0]) {

		fmt.Print(`Usage:
pantahub-base-kafka [--baseurl <baseapiurl>] [--topic <topicname>] [--consumer-group <groupname>] <controller> 
	  
Available controllers are: steps and devices.

Env Vars:
1.PANTAHUB_BASE_URL
2.KAFKA_TOPIC
3.KAFKA_CONSUMER_GROUP
4.KAFKA_HOST
5.KAFKA_PORT
	 `)

		return
	}

	controllerName := flag.Args()[0]

	if controllerName == "devices" {
		ns := controllers.NewDeviceProcessor(*topic, *consumerGroup)
		ns.Run()
	} else if controllerName == "steps" {
		ns := controllers.NewStepProcessor(*topic, *consumerGroup)
		ns.Run()
	}
}

// Contains check if an item exists in an array of strings
func Contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}
